package org.briarproject.briar.android.conversation;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import org.briarproject.bramble.api.nullsafety.NotNullByDefault;
import org.briarproject.bramble.api.sync.MessageId;

import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.concurrent.Immutable;

@Immutable
@NotNullByDefault
public class AttachmentItem implements Parcelable {

	private final MessageId messageId;
	private final int width, height;
	private final String mimeType, extension;
	private final int thumbnailWidth, thumbnailHeight;
	private final boolean hasError;
	private final long instanceId;

	public static final Creator<AttachmentItem> CREATOR =
			new Creator<AttachmentItem>() {
				@Override
				public AttachmentItem createFromParcel(Parcel in) {
					return new AttachmentItem(in);
				}

				@Override
				public AttachmentItem[] newArray(int size) {
					return new AttachmentItem[size];
				}
			};

	private static final AtomicLong NEXT_INSTANCE_ID = new AtomicLong(0);

	AttachmentItem(MessageId messageId, int width, int height, String mimeType,
			String extension, int thumbnailWidth, int thumbnailHeight,
			boolean hasError) {
		this.messageId = messageId;
		this.width = width;
		this.height = height;
		this.mimeType = mimeType;
		this.extension = extension;
		this.thumbnailWidth = thumbnailWidth;
		this.thumbnailHeight = thumbnailHeight;
		this.hasError = hasError;
		instanceId = NEXT_INSTANCE_ID.getAndIncrement();
	}

	protected AttachmentItem(Parcel in) {
		byte[] messageIdByte = new byte[MessageId.LENGTH];
		in.readByteArray(messageIdByte);
		messageId = new MessageId(messageIdByte);
		width = in.readInt();
		height = in.readInt();
		mimeType = in.readString();
		extension = in.readString();
		thumbnailWidth = in.readInt();
		thumbnailHeight = in.readInt();
		hasError = in.readByte() != 0;
		instanceId = in.readLong();
	}

	public MessageId getMessageId() {
		return messageId;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

	String getMimeType() {
		return mimeType;
	}

	String getExtension() {
		return extension;
	}

	int getThumbnailWidth() {
		return thumbnailWidth;
	}

	int getThumbnailHeight() {
		return thumbnailHeight;
	}

	boolean hasError() {
		return hasError;
	}

	String getTransitionName() {
		return String.valueOf(instanceId);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByteArray(messageId.getBytes());
		dest.writeInt(width);
		dest.writeInt(height);
		dest.writeString(mimeType);
		dest.writeString(extension);
		dest.writeInt(thumbnailWidth);
		dest.writeInt(thumbnailHeight);
		dest.writeByte((byte) (hasError ? 1 : 0));
		dest.writeLong(instanceId);
	}

	@Override
	public boolean equals(@Nullable Object o) {
		return o instanceof AttachmentItem &&
				instanceId == ((AttachmentItem) o).instanceId;
	}

}
